#!/usr/bin/python3

from gendev_tools.nat_mch.nat_mch import NATMCH, BackplaneType
from gendev_tools.gendev_interface import ConnType

mchs = ["tgt-twds1000-ctrl-mch-01.cslab.esss.lu.se"]
target_fw = "V2.21.8"

for mch in mchs:
    this_mch = NATMCH(
        ip_address=mch,
        allowed_conn=[ConnType.TELNET],
        backplane=BackplaneType.B3U,
    )
    valid, mchconfig = this_mch.device_info()
    cur_fw = mchconfig["Board"]["fw_ver"]
    if valid:
        print("{} -> {}".format(mch, cur_fw))
        if target_fw != cur_fw:
            print("Firmware update required. This will take a few minutes...")
            good, response = this_mch.update_fw(target_fw[1:])
            if good is False:
                print("Firmware update failed for {}:".format(mch))
                print("    {}".format(response))
            else:
                print("Firmware update succeeded for {}".format(mch))
                this_mch._tel_conn._reboot_all_slots()
        elif target_fw == cur_fw:
            print("Firmware is up-to-date ({})".format(cur_fw))
    else:
        print("Failed to get board info for {}".format(mch))
    print("")
